public class Arrays_NewYearChaos
{
    static void minimumBribes(int[] q)
    {
        int bribes = 0;
        int difference = 0;
        boolean flag = true;

        for (int i = 0; i < q.length; i++)
        {
            difference = q[i] - (i + 1);

            if (difference > 2)
            {
                System.out.println("Too chaotic");
                flag = false;
                break;
            }

            bribes += Math.abs(difference);
        }
        if (flag)
        {
            System.out.println(bribes / 2);
        }
    }
}
