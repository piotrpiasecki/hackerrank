public class T1_SockMerchant
{
    static int sockMerchant(int n, int[] ar)
    {
        int[] sockPiles = new int[101];

        int pairs = 0;

        for (int i = 0; i < ar.length; i++)
        {
            sockPiles[ar[i]]++;
        }

        for (int i = 0; i < sockPiles.length; i++)
        {
            pairs += sockPiles[i] / 2;
        }
        return pairs;
    }
}
