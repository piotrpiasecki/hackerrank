import java.util.ArrayList;
import java.util.List;

public class Q1_Count
{
    public static int countNonUnique(List<Integer> numbers)
    {

        int nonUnique = 0;
        int[] integers = new int[numbers.size() + 1];
        //Arrays.fill(integers, 0);

        for (int i = 0; i < numbers.size(); i++)
        {
            if (integers[numbers.get(i)] == 0)
            {
                integers[numbers.get(i)] = 1;
            }
            else
            {
                nonUnique++;
            }
        }
        return nonUnique;
    }

    public static void main(String[] args)
    {
        List<Integer> intList = new ArrayList<>();
        intList.add(8);
        intList.add(1);
        intList.add(3);
        intList.add(1);
        intList.add(4);
        intList.add(5);
        intList.add(6);
        intList.add(3);
        intList.add(2);
        System.out.println(countNonUnique(intList));
    }
}
