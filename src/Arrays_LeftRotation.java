import java.util.LinkedList;

/**
 * A left rotation operation on an array shifts each of the array's elements  unit to the left. For example, if  left rotations are performed on array , then the array would become .
 *
 * Given an array  of  integers and a number, , perform  left rotations on the array. Return the updated array to be printed as a single line of space-separated integers.
 *
 * Function Description
 *
 * Complete the function rotLeft in the editor below. It should return the resulting array of integers.
 *
 * rotLeft has the following parameter(s):
 *
 * An array of integers .
 * An integer , the number of rotations.
 */

public class Arrays_LeftRotation
{
    static int[] rotLeft(int[] a, int d)
    {

        if ((a.length != 0) && (d % a.length != 0))
        {
            d %= a.length;

            LinkedList<Integer> list = new LinkedList<>();

            for (int i : a)
            {
                list.add(i);
            }

            for (int i = 0; i < d; i++)
            {
                list.addLast(list.getFirst());
                list.removeFirst();
            }

            for (int i = 0; i < a.length; i++)
            {
                a[i] = list.get(i);
            }
        }
        return a;
    }
}
