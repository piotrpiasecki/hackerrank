import java.util.Arrays;

public class T4_RepeatedString
{
    static long repeatedString(String s, long n)
    {
        int sLength = s.length();
        long noOfaLettersInString = 0;
        long noOfaLetters = 0;

        if (n <= sLength)
        {
            for (int i = 0; i < n; i++)
            {
                if (s.charAt(i) == 'a')
                {
                    noOfaLetters++;
                }
            }
        }
        else
        {
            int[] aArray = new int[sLength];

            for (int i = 0; i < sLength; i++)
            {
                if (s.charAt(i) == 'a')
                {
                    noOfaLettersInString++;
                    aArray[i] = 1;
                }
            }

            noOfaLetters += (n / sLength) * noOfaLettersInString;

            long remaining = n % sLength;

            for (int i = 0; i < remaining; i++)
            {
                if (s.charAt(i) == 'a')
                {
                    noOfaLetters++;
                }
            }

        }
    return noOfaLetters;
    }

    public static void main(String[] args)
    {
        long n = 1L;
        String s = "dfgfdg";

        System.out.println(repeatedString(s,n));
    }
}
