/**
 * Crude and quite ugly solution for C++ compiler braces checker
 */

public class Q1_Braces_1
{
    static String[] braces(String[] values)
    {

        String[] result = new String[values.length];
        char formerChar = '{';
        char tempChar = '{';
        int left;
        int right;

        for (int i = 0; i < values.length; i++)
        {
            left = 0;
            right = 0;

            if (values[i].length() % 2 == 1)
            {
                result[i] = "NO";
                break;
            }

            for (int k = 0; k < values[i].length(); k++)
            {
                if (values[i].charAt(k) == '{'
                || values[i].charAt(k) == '['
                || values[i].charAt(k) == '(')
                {
                    left++;
                }
                else if (values[i].charAt(k) == '}'
                    || values[i].charAt(k) == ']'
                    || values[i].charAt(k) == ')')
                {
                    right++;
                }
            }

            if (left != right)
            {
                result[i] = "NO";
                continue;
            }
            else
            {
                result[i] = "YES";

                for (int j = 1; j < values[i].length(); j++)
                {
                    formerChar = values[i].charAt(j - 1);
                    tempChar = values[i].charAt(j);

                    if (formerChar == '{' && (tempChar == '[' || tempChar == '}' || tempChar == '{' || tempChar == '('))
                    {
                        continue;
                    }
                    else if (formerChar == '[' && (tempChar == '(' || tempChar == ']' || tempChar == '['))
                    {
                        continue;
                    }
                    else if (formerChar == '(' && (tempChar == '(' || tempChar == ')' || tempChar == '{' || tempChar == '['))
                    {
                        continue;
                    }
                    else if (formerChar == ')' && (tempChar == ')' || tempChar == '(' || tempChar == ']' || tempChar == '}' || tempChar == '{' || tempChar == '['))
                    {
                        continue;
                    }
                    else if (formerChar == ']' && (tempChar == '[' || tempChar == '}' || tempChar == '(' || tempChar == ']' || tempChar == ')'))
                    {
                        continue;
                    }
                    else if (formerChar == '}' && (tempChar == '{' || tempChar == '(' || tempChar == '[' || tempChar == '}'))
                    {
                        continue;
                    }
                    else
                    {
                        result[i] = "NO";
                        break;
                    }
                }
            }
        }
        return result;
    }

    public static void main(String[] args)
    {
        String[] braces =
                {
                        "}][}}(}][))]", //NO
                        "[](){()}", // YES
                        "()", //YES
                        "({}([][]))[]()", //YES
                        "{)[](}]}]}))}(())(", //NO
                        "([[)", //NO
                };
//        String[] Q1_Braces_1 = {"({}([][]))[]()"};

//        String[] Q1_Braces_1 =
//                {
//                        "{[()]}", //YES
//                        "{[(])}", //NO
//                        "{{[[(())]]}}", //YES
//                };

        String[] result = braces(braces);

        for (String string : result)
        {
            System.out.println(string);
        }
    }
}
