/**
 * More efficient and definitely more elegant solution
 */

import java.util.LinkedList;

public class Q1_Braces_2
{
    static String[] braces(String[] values)
    {

        String[] result = new String[values.length];
        LinkedList<Character> chars;
        char tempChar;
        char currentChar;
        boolean matching = false;

        for (int i = 0; i < values.length; i++)
        {
            chars = new LinkedList<>();
            chars.add(values[i].charAt(0));

            for (int j = 1; j < values[i].length(); j++)
            {
                if (chars.size() != 0)
                {
                    tempChar = chars.getLast();
                    currentChar = values[i].charAt(j);
                }
                else
                {
                    currentChar = values[i].charAt(j);
                    tempChar = currentChar;
                }

                if ((tempChar == '{' && currentChar == '}')
                    || (tempChar == '[' && currentChar == ']')
                    || (tempChar == '(' && currentChar == ')'))
                {
                    matching = true;
                }
                else
                {
                    matching = false;
                }

                if (matching)
                {
                    chars.removeLast();
                }
                else
                {
                    chars.addLast(values[i].charAt(j));
                }
            }

            if (chars.size() == 0)
            {
                result[i] = "YES";
            }
            else
            {
                result[i] = "NO";
            }
        }
        return result;
    }

    public static void main(String[] args)
    {
        String[] braces =
                {
                        "}][}}(}][))]", //NO
                        "[](){()}", // YES
                        "()", //YES
                        "({}([][]))[]()", //YES
                        "{)[](}]}]}))}(())(", //NO
                        "([[)", //NO
                };
//        String[] Q1_Braces_1 = {"({}([][]))[]()"};

//        String[] Q1_Braces_1 =
//                {
//                        "{[()]}", //YES
//                        "{[(])}", //NO
//                        "{{[[(())]]}}", //YES
//                };

        String[] result = braces(braces);

        for (String string : result)
        {
            System.out.println(string);
        }
    }
}
