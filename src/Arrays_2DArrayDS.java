/**
 * Given a  2D Array, :
 * <p>
 * 1 1 1 0 0 0
 * 0 1 0 0 0 0
 * 1 1 1 0 0 0
 * 0 0 0 0 0 0
 * 0 0 0 0 0 0
 * 0 0 0 0 0 0
 * We define an hourglass in  to be a subset of values with indices falling in this pattern in 's graphical representation:
 * <p>
 * a b c
 * d
 * e f g
 * There are  hourglasses in , and an hourglass sum is the sum of an hourglass' values. Calculate the hourglass sum for every hourglass in , then print the maximum hourglass sum.
 * <p>
 * For example, given the 2D array:
 * <p>
 * -9 -9 -9  1 1 1
 * 0 -9  0  4 3 2
 * -9 -9 -9  1 2 3
 * 0  0  8  6 6 0
 * 0  0  0 -2 0 0
 * 0  0  1  2 4 0
 * We calculate the following  hourglass values:
 * <p>
 * -63, -34, -9, 12,
 * -10, 0, 28, 23,
 * -27, -11, -2, 10,
 * 9, 17, 25, 18
 * Our highest hourglass value is  from the hourglass:
 * <p>
 * 0 4 3
 * 1
 * 8 6 6
 */


public class Arrays_2DArrayDS
{
    static int hourglassSum(int[][] arr)
    {
        int tempHourglass;
        int maxHourglass = Integer.MIN_VALUE;
        int counter = 0;

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                tempHourglass = 0;
                tempHourglass += arr[i][j];
                tempHourglass += arr[i][j + 1];
                tempHourglass += arr[i][j + 2];
                tempHourglass += arr[i + 1][j + 1];
                tempHourglass += arr[i + 2][j];
                tempHourglass += arr[i + 2][j + 1];
                tempHourglass += arr[i + 2][j + 2];
                counter++;

                if (tempHourglass > maxHourglass)
                {
                    maxHourglass = tempHourglass;
                }
            }
        }
        System.out.println(counter);
        return maxHourglass;
    }

    public static void main(String[] args)
    {
        int[][] arr = {
                {1, 1, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {1, 1, 1, 0, 0, 0},
                {0, 0, 2, 4, 4, 0},
                {0, 0, 0, 2, 0, 0},
                {0, 0, 1, 2, 4, 0}};

        System.out.println(hourglassSum(arr));
        }
    }
