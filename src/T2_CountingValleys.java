public class T2_CountingValleys
{
    static int countingValleys(int n, String s)
    {
        int noOfValleys = 0;
        int tempSum = 0;
        int formerlySum = 0;

        for (int i = 0; i < s.length(); i++)
        {
            switch (s.charAt(i))
            {
                case 'D' :
                    tempSum--;
                    break;
                case 'U' :
                    tempSum++;
                    break;
            }

            if (tempSum == 0 && formerlySum < 0)
            {
                noOfValleys++;
            }
            formerlySum = tempSum;
        }

        return noOfValleys;
    }
}
